import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../../services/local-storage/local-storage.service';
import {UserModel} from '../../../models/user/user.model';
import {List} from '../../../models/pokemon-list/pokemon-list';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginFormComponent {

  public username: string
  public errorMsg: string
  constructor(private router: Router, private storageService: LocalStorageService) {
  }

  onLogin(name: string){
    if (name === undefined) {
      this.errorMsg = "Enter a valid username!"
    } else {
      const collection: List[] = []
      this.storageService.set(this.storageService.USER_KEY, {name, collection})
      this.router.navigateByUrl('/catalogue')
    }
  }

  ngOnInit() {
    if(this.storageService.get(this.storageService.USER_KEY) !== null){
      this.router.navigateByUrl("/catalogue")
    }
  }
}
