import {NgModule} from '@angular/core';
import {TrainerProfilePage} from './pages/trainer-profile.page';
import {TrainerProfileRoutingModule} from './trainer-profile-routing.module';
import {PokemonCollectionComponent} from './components/pokemon-collection/pokemon-collection.component';
import {CommonModule} from '@angular/common';
import {CatalogueModule} from '../catalogue/catalogue.module';

@NgModule({
  declarations: [
    TrainerProfilePage,
    PokemonCollectionComponent
  ],
  imports: [
    TrainerProfileRoutingModule,
    CommonModule,
    CatalogueModule
  ]
})

export class TrainerProfileModule {}
