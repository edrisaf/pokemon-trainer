import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TrainerProfilePage} from './pages/trainer-profile.page';

const routes: Routes = [
  {
    path: '',
    component: TrainerProfilePage,
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class TrainerProfileRoutingModule {}
