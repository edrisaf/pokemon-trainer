import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from '../../../services/local-storage/local-storage.service';
import {UserModel} from '../../../models/user/user.model';
import {List} from '../../../models/pokemon-list/pokemon-list';

@Component({
  selector: 'app-pokemon-collection',
  templateUrl: './pokemon-collection.component.html',
  styleUrls: ['./pokemon-collection.component.css']
})

export class PokemonCollectionComponent {
  public collection :List[];
  public username: string;
  constructor(private storageService: LocalStorageService) {
  }

  ngOnInit() {
    const user : UserModel = this.storageService.get(this.storageService.USER_KEY)
    this.username = user.name;
    this.collection = user.collection;
  }

}
