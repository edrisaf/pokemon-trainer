import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';
import {UserModel} from '../../models/user/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private storage: LocalStorageService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
  {
    let data : UserModel;
    data = this.storage.get(this.storage.USER_KEY);
    if (data !== null)
      return true;
    else {
      this.router.navigateByUrl("/login");
      return false;
    }
  }

}
