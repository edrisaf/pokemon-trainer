import {Component} from '@angular/core';
import {LocalStorageService} from '../../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
  constructor(public storageService: LocalStorageService) {
  }
}
