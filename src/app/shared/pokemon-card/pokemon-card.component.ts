import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {List} from '../../models/pokemon-list/pokemon-list';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})

export class PokemonCardComponent {
  @Input('pokemon') card : List;

  constructor(private router: Router) {
  }

  pokemonClicked(name: string){
    this.router.navigateByUrl('/pokemon/' + name)
  }
}

