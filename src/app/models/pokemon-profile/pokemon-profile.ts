export interface PokemonProfile
{
  name: string;
  sprites: {front_default: string};
  stats: {
    base_stat: number;
    effort: number;
    stat: {
      name: string;
    }
  }[];
  height: number;
  weight: number;
  abilities: {
    ability:{
      name: string;
    }
  }[];
  base_experience: number;
  moves: {
    move:{
      name: string;
    }
  }[];
  types: {
    slot: number;
    type: {
      name: string;
      url: string;
    }
  }[];
}
