import {List} from '../pokemon-list/pokemon-list';

export interface UserModel
{
  name: string;
  collection: List[];
}
