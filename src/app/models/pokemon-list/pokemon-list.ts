export interface PokemonList
{
  results: List[];
}

export interface List
{
  name: string;
  url: string;
  image: string;
}
