import {NgModule} from '@angular/core';
import {PokemonDetailsPage} from './pages/pokemon-details.page';
import {PokemonDetailsComponent} from './components/pokemon-details.component';
import {PokemonDetailsRoutingModule} from './pokemon-details-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    PokemonDetailsPage,
    PokemonDetailsComponent
  ],
  imports: [
    PokemonDetailsRoutingModule,
    CommonModule
  ]
})

export class PokemonDetailsModule {}
