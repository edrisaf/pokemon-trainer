import {Component, OnInit} from '@angular/core';
import {PokeapiService} from '../../services/pokemon/pokeapi.service';
import {ActivatedRoute} from '@angular/router';
import {PokemonProfile} from '../../models/pokemon-profile/pokemon-profile';
import {List} from '../../models/pokemon-list/pokemon-list';
import {LocalStorageService} from '../../services/local-storage/local-storage.service';
import {UserModel} from '../../models/user/user.model';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css']
})

export class PokemonDetailsComponent {
  public pokemonName
  public pokemon : PokemonProfile
  public caught : boolean;
  constructor(private pokeApi: PokeapiService, private route: ActivatedRoute, private storageService: LocalStorageService) {
  }

  ngOnInit(){
    this.pokemonName = this.route.snapshot.paramMap.get('name')
    this.pokeApi.getDetails(this.pokemonName).subscribe(data => {
      this.pokemon = data
    })
  }

  collectPokemon()
  {
    let user : UserModel = this.storageService.get(this.storageService.USER_KEY)
    const collectedPokemon : List = {name: this.pokemon.name, url: "PokemonURL", image: this.pokemon.sprites.front_default};
    user.collection.push(collectedPokemon);
    this.storageService.set(this.storageService.USER_KEY,user);
    this.caught = true;
  }
}
