import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PokemonDetailsPage} from './pages/pokemon-details.page';


const routes: Routes = [
  {
    path: '',
    component: PokemonDetailsPage
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PokemonDetailsRoutingModule {}
