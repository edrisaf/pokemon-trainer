import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class LocalStorageService
{
  public USER_KEY = "KEY-USER-OBJ";

  constructor() {}

  get(key: string) : any
  {
    const stored = localStorage.getItem(key);
    if (!stored) return null;
    else return JSON.parse(stored);
  }

  set(key: string, value: any)
  {
    try
    {
      const json = JSON.stringify(value);
      localStorage.setItem(key, json);
    }
    catch (e)
    {
      console.log(e.message());
    }
  }

  clear()
  {
    localStorage.clear();
  }

}


