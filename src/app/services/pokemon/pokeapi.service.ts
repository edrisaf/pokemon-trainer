import {List, PokemonList} from '../../models/pokemon-list/pokemon-list';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {PokemonProfile} from '../../models/pokemon-profile/pokemon-profile';

@Injectable({providedIn: 'root'})
export class PokeapiService {
  url = "https://pokeapi.co/api/v2/";
  constructor(private http:HttpClient) {
  }

  getList(link: string) : Observable<PokemonList>
  {
    return this.http.get<PokemonList>((link) ? link : this.url + "pokemon?limit=32offset=0").pipe(map(res => res));
  }

  getPokemonImages(responseData: PokemonList) : List[]
  {
    let result = responseData.results;
    for (let i = 0; i < result.length; i++)
    {
      const id = responseData.results[i].url.split( '/' ).filter( Boolean ).pop();
      result[i].image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`;
    }
    return result;
  }

  getDetails(name: string): Observable<PokemonProfile>
  {
    return this.http.get<PokemonProfile>(this.url + "pokemon/" + name).pipe(map(res => res));
  }
}


