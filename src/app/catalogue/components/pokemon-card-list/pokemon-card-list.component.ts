import {Component, OnInit} from '@angular/core';
import {PokeapiService} from '../../../services/pokemon/pokeapi.service';
import {List} from '../../../models/pokemon-list/pokemon-list';

@Component({
  selector: 'app-pokemon-card-list',
  templateUrl: './pokemon-card-list.component.html',
  styleUrls: ['./pokemon-card-list.component.css']
})

export class PokemonCardListComponent {
  public pokemonList: List[]
  public pokemonResponse;
  constructor(private pokemonApi: PokeapiService) {
  }

  ngOnInit(){
   this.pokemonApi.getList(null).subscribe(data => {
     this.pokemonList = this.pokemonApi.getPokemonImages(data)
     this.pokemonResponse = data;
   })
  }

  browsePrev()
  {
    this.pokemonApi.getList(this.pokemonResponse.previous).subscribe(data => {
      this.pokemonList = this.pokemonApi.getPokemonImages(data)
      this.pokemonResponse = data;
      window.scrollTo(0, 0);
    })
  }
  browseNext()
  {
    this.pokemonApi.getList(this.pokemonResponse.next).subscribe(data => {
      this.pokemonList = this.pokemonApi.getPokemonImages(data)
      this.pokemonResponse = data;
      window.scrollTo(0, 0);
    })
  }

}
