import { NgModule } from '@angular/core';
import {CataloguePage} from './pages/catalogue.page';
import {CatalogueRoutingModule} from './catalogue-routing.modue';
import {PokemonCardComponent} from '../shared/pokemon-card/pokemon-card.component';
import {PokemonCardListComponent} from './components/pokemon-card-list/pokemon-card-list.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    CataloguePage,
    PokemonCardComponent,
    PokemonCardListComponent
  ],
  exports: [
    PokemonCardComponent
  ],
  imports: [
    CatalogueRoutingModule,
    CommonModule
  ]
})

export class CatalogueModule {}
